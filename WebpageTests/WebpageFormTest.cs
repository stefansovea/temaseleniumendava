﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace WebpageTests
{
    public class WebpageFormTest
    {
        const string homeURL = "https://demoqa.com/automation-practice-form";
        const string homeTitle = "ToolsQA";

        [Fact]
        [Trait("Category","Smoke")]
        public void LoadApplicationPage()
        {
            using (IWebDriver driver = new ChromeDriver())
            {
                driver.Navigate().GoToUrl(homeURL);
                Assert.Equal(homeTitle, driver.Title);
                Assert.Equal(homeURL, driver.Url);
            }
        }    

        [Fact]
        [Trait("Category", "Input")]
        public void FormSubmitTest()
        {
            using (IWebDriver driver = new ChromeDriver())
            {
                driver.Navigate().GoToUrl(homeURL);
                driver.FindElement(By.Id("firstName")).SendKeys("Steven");
                PauseHelper.Pause();
                driver.FindElement(By.Id("lastName")).SendKeys("Hawk");
                PauseHelper.Pause();
                driver.FindElement(By.Id("userEmail")).SendKeys("stevenhawk@outlook.com");
                PauseHelper.Pause();
                driver.FindElement(By.ClassName("custom-control-label")).Click();
                PauseHelper.Pause();
                driver.FindElement(By.Id("userNumber")).SendKeys("0727773777");
                PauseHelper.Pause();
                driver.FindElement(By.Id("dateOfBirthInput")).SendKeys("2.08.2021");
                PauseHelper.Pause();
              //driver.FindElement(By.XPath("//*[@id='subjectsContainer']/div/div[1]")).SendKeys("Physics"); 
              //PauseHelper.Pause();
                driver.FindElement(By.XPath("//*[@id='hobbies-checkbox-1']")).Submit();
                PauseHelper.Pause();
                driver.FindElement(By.Id("currentAddress")).SendKeys("Str. trandafirilor nr. 20");
                PauseHelper.Pause();
                

                IWebElement stateCmbBox = driver.FindElement(By.Id("state"));
                SelectElement contState = new SelectElement(stateCmbBox);
                contState.SelectByText("NCR");
                PauseHelper.Pause();
                IWebElement cityCmbBox = driver.FindElement(By.Id("city"));
                SelectElement contCity = new SelectElement(cityCmbBox);
                contCity.SelectByText("Delhi");
                PauseHelper.Pause();     
                
                driver.FindElement(By.XPath("//*[@id='submit']")).Click();
                PauseHelper.Pause();
                driver.FindElement(By.XPath("//*[@id='closeLargeModal']")).Click();
                PauseHelper.Pause(); 
            }
        }
    }
}
