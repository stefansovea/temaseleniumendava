﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WebpageTests
{
    internal static class PauseHelper
    {
        public static void Pause (int secondsToPause = 1500)
        {
            Thread.Sleep(secondsToPause);
        }
    }
}
